# Applied Security Lab
## Vagrant

For all vagrant commands, you need to be in the vagrant directory.

To create all three VMs: `vagrant up`. This can take some time the first time.

To create a single VM: `vagrant up [client|server|backup]`. Vagrant automatically installs keys for ssh authentication. Use `vagrant ssh [client|server|backup]` to connect via ssh to a VM. Use `vagrant destroy [client|server|backup]` to destroy a VM. You can create it again with `vagrant up`.

