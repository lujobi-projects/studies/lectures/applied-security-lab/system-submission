from cryptography.hazmat.primitives.asymmetric.ec import SECP384R1, generate_private_key, ECDSA
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives.hashes import SHA1, SHA256
from cryptography import x509
from cryptography.x509.base import Certificate, RevokedCertificate, load_pem_x509_certificate, load_pem_x509_crl
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives.serialization import (
    Encoding,
    PrivateFormat,
    NoEncryption,
    BestAvailableEncryption,
    load_pem_private_key,
    load_pem_public_key,
    load_ssh_public_key,
)
from cryptography.hazmat.primitives.serialization.pkcs12 import serialize_key_and_certificates
from cryptography.exceptions import InvalidSignature

from base64 import b64encode, b64decode

from typing import Tuple

from datetime import datetime, timedelta
import os
from shutil import move

from flask_restx.fields import Boolean

from api.utils import update_get_serial_number, update_issued_new_certificate, update_revoked_certificate

# basePath = os.path.dirname(os.path.realpath(__file__))
basePath = "/api/certs"

ROOT_CERT = basePath + "/rootCA.pem"
ROOT_KEY_PRIV = basePath + "/rootCA-key.pem"
USER_ADMIN_KEY = basePath + "/encryption_key.pem"
USER_CERT_PATH = basePath + "/user_certs/"
USER_KEY_PATH = basePath + "/user_keys/"
# SERIAL_NUMBER_FILE = basePath + "/../certs/serialNumber.txt"
CRL_DIR = basePath + "/revokation_lists/"
REVOKED_DIR = basePath + "/revoked/"


class ExistingCertificate(Exception):
    pass


class CertificateAlreadyRevoked(Exception):
    pass


class CannotFindCertificate(Exception):
    pass


class InvalidCertificate(Exception):
    pass


def __getNextSerialNumber():
    # with open(SERIAL_NUMBER_FILE, 'rb') as fh:
    #     curNumber = int(fh.read(), 0)
    # curNumber += 1
    # with open(SERIAL_NUMBER_FILE, 'w') as fh:
    #     fh.write(hex(curNumber))
    return update_get_serial_number()


def __loadPrivKey(filename: str):
    with open(filename, "rb") as fh:
        rawKey = fh.read()
    privKey = load_pem_private_key(rawKey, None)
    return privKey


def __savePrivKey(key, filename: str):
    rawKey = key.private_bytes(
        encoding=Encoding.PEM, format=PrivateFormat.TraditionalOpenSSL, encryption_algorithm=NoEncryption()
    )
    with open(filename, "wb") as fh:
        fh.write(rawKey)


def __loadPubKey(filename: str):
    with open(filename, "rb") as fh:
        rawKey = fh.read()
    pubKey = load_pem_public_key(rawKey, None)
    # pubKey = load_ssh_public_key(rawKey, None)
    return pubKey


def __loadCert(filename: str):
    with open(filename, "rb") as fh:
        rawCert = fh.read()
    cert = x509.load_pem_x509_certificate(rawCert)
    return cert


def __saveCert(cert: Certificate, filename: str):
    rawCert = cert.public_bytes(Encoding.PEM)
    with open(filename, "wb") as fh:
        fh.write(rawCert)


def __loadRootCert():
    return __loadCert(ROOT_CERT)


def getPemRootCert():
    return __loadRootCert().public_bytes(Encoding.PEM)


def getChallenge():
    return b64encode(os.urandom(48))


def __loadCrl():
    if not os.path.exists(CRL_DIR + "last.crl"):
        return __generateEmptyCrl().sign(__loadPrivKey(ROOT_KEY_PRIV), SHA1())
    with open(CRL_DIR + "last.crl", "rb") as fh:
        rawCrl = fh.read()
    crl = load_pem_x509_crl(rawCrl, None)
    return crl


def __saveCrl(crl):
    rawCrl = crl.public_bytes(encoding=Encoding.PEM)
    with open(CRL_DIR + "last.crl", "wb") as fh:
        fh.write(rawCrl)
    with open(CRL_DIR + crl.last_update.strftime("%m_%d_%Y_%H_%M_%S") + ".crl", "wb") as fh:
        fh.write(rawCrl)


def getPemCrl():
    return __loadCrl().public_bytes(Encoding.PEM)


def __generateEmptyCrl():
    # Create new empty CRL

    issuer = x509.Name(
        [
            x509.NameAttribute(NameOID.COUNTRY_NAME, "CH"),
            x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, "Zuerich"),
            x509.NameAttribute(NameOID.LOCALITY_NAME, "Zuerich"),
            x509.NameAttribute(NameOID.COMMON_NAME, "iMovies"),
            x509.NameAttribute(NameOID.EMAIL_ADDRESS, "admin@imovies.ch"),
        ]
    )

    newCrl = (
        x509.CertificateRevocationListBuilder()
        .issuer_name(issuer)
        .last_update(datetime.now())
        .next_update(datetime.now() + timedelta(0, 0, 1))
    )

    return newCrl


def validateChallenge(signature: bytes, uid: str, token: str) -> Boolean:
    token = b64decode(token)
    cert = __loadCert(USER_CERT_PATH + f"{uid}.crt")
    publicKey = cert.public_key()
    try:
        publicKey.verify(signature, token, ECDSA(SHA256()))
    except InvalidSignature:
        return False
    return True


def revokeCertificate(uid: str):
    # load certificate

    if not os.path.exists(USER_CERT_PATH + f"{uid}.crt"):
        raise CannotFindCertificate()

    cert = __loadCert(USER_CERT_PATH + f"{uid}.crt")

    # Test if certificate is already revoked

    oldCrl = __loadCrl()

    if oldCrl.get_revoked_certificate_by_serial_number(cert.serial_number) is not None:
        raise CertificateAlreadyRevoked()

    # Create new empty CRL

    newCrl = __generateEmptyCrl()

    # Add all previously revoked certificates to new crl

    for revoked_cert in oldCrl:
        newCrl.add_revoked_certificate(revoked_cert)

    # add new certificate to crl

    revoked_cert = (
        x509.RevokedCertificateBuilder().serial_number(cert.serial_number).revocation_date(datetime.now()).build()
    )

    newCrl.add_revoked_certificate(revoked_cert)
    signedCrl = newCrl.sign(__loadPrivKey(ROOT_KEY_PRIV), SHA1())

    update_revoked_certificate()

    __saveCrl(signedCrl)

    if os.path.exists(USER_KEY_PATH + f"{uid}.key"):
        move(USER_KEY_PATH + f"{uid}.key", REVOKED_DIR + f"{uid}.key")

    if os.path.exists(USER_CERT_PATH + f"{uid}.crt"):
        move(USER_CERT_PATH + f"{uid}.crt", REVOKED_DIR + f"{uid}.crt")


def verifyClientCert(pemCert: bytes):
    cert = load_pem_x509_certificate(pemCert)
    crl = __loadCrl()

    if crl.get_revoked_certificate_by_serial_number(cert.serial_number) is not None:
        print("Revoked")
        raise InvalidCertificate("Certificate revoked")

    if cert.not_valid_after < datetime.now():
        print("Expired")
        raise InvalidCertificate("Certificate expired")

    if cert.not_valid_before > datetime.now():
        print("Too early")
        raise InvalidCertificate("Certificate not yet valid")

    rootCert = __loadRootCert()

    try:
        rootCert.public_key().verify(
            cert.signature,
            cert.tbs_certificate_bytes,
            padding.PKCS1v15(),
            cert.signature_hash_algorithm,
        )
        # rootCert.public_key().verify(cert.signature, cert.tbs_certificate_bytes, SHA1())
    except InvalidSignature:
        print("Invalid signature")
        raise InvalidCertificate("Invalid signature")

    uid = cert.subject.get_attributes_for_oid(NameOID.COMMON_NAME)
    return uid[0].value


def generateCertificateForUser(uid: str, email: str, firstname: str, lastname: str) -> Tuple[bytes, bytes]:

    # check if the user already has a key and certificate
    if os.path.exists(USER_CERT_PATH + f"{uid}.crt") or os.path.exists(USER_KEY_PATH + f"{uid}.key"):
        raise ExistingCertificate()

    # Cenerate a private key for the requested user
    curve = SECP384R1
    privKey = generate_private_key(curve)

    # Create certificate
    issuer = x509.Name(
        [
            x509.NameAttribute(NameOID.COUNTRY_NAME, "CH"),
            x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, "Zuerich"),
            x509.NameAttribute(NameOID.LOCALITY_NAME, "Zuerich"),
            x509.NameAttribute(NameOID.COMMON_NAME, "iMovies"),
            x509.NameAttribute(NameOID.EMAIL_ADDRESS, "admin@imovies.ch"),
        ]
    )

    subject = x509.Name(
        [
            x509.NameAttribute(NameOID.COUNTRY_NAME, "CH"),
            x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, "Zuerich"),
            x509.NameAttribute(NameOID.LOCALITY_NAME, "Zuerich"),
            x509.NameAttribute(NameOID.COMMON_NAME, uid),
            x509.NameAttribute(NameOID.EMAIL_ADDRESS, email),
            x509.NameAttribute(NameOID.SURNAME, lastname),
            x509.NameAttribute(NameOID.GIVEN_NAME, firstname),
        ]
    )

    serialNumber = __getNextSerialNumber()

    cert = (
        x509.CertificateBuilder()
        .subject_name(subject)
        .issuer_name(issuer)
        .serial_number(serialNumber)
        .public_key(privKey.public_key())
        .not_valid_before(datetime.utcnow())
        .not_valid_after(datetime.utcnow() + timedelta(days=365))
        .sign(__loadPrivKey(ROOT_KEY_PRIV), SHA1())
    )

    # Convert the private key to OpenSSL PEM format
    rawKey = privKey.private_bytes(
        encoding=Encoding.PEM, format=PrivateFormat.TraditionalOpenSSL, encryption_algorithm=NoEncryption()
    )

    # Convert certificate to PEM format and save to file
    rawCert = cert.public_bytes(Encoding.PEM)

    __saveCert(cert, USER_CERT_PATH + f"{uid}.crt")
    update_issued_new_certificate()

    # Encrypt the private key and save to file
    encryptionKey = __loadPubKey(USER_ADMIN_KEY)
    cipherKey = encryptionKey.encrypt(
        rawKey, padding.OAEP(mgf=padding.MGF1(algorithm=SHA256()), algorithm=SHA256(), label=None)
    )

    with open(USER_KEY_PATH + f"{uid}.key", "wb") as fh:
        fh.write(cipherKey)

    pkcs12 = serialize_key_and_certificates(uid.encode(), privKey, cert, None, BestAvailableEncryption(b"iMovies"))

    with open(USER_KEY_PATH + f"{uid}.pkcs", "wb") as fh:
        fh.write(pkcs12)

    return pkcs12
