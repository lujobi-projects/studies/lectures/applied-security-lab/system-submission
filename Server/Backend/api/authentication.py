import functools  # noqa: TAE001

from flask_restx import Namespace
from werkzeug.exceptions import Unauthorized

from .utils import check_session_token, get_user, is_admin
from cryptoModule.certs import verifyClientCert

from urllib.parse import unquote


def login_required(api: Namespace, cert_only=False, admin_only=False):  # noqa: ANN201
    def decorator(func):  # noqa: ANN001
        @functools.wraps(func)  # noqa: ANN201
        def wrapped(*f_args, **f_kwargs):  # noqa: ANN002, ANN003, ANN201
            parser = api.parser()
            parser.add_argument("Authorization", type=str, location="headers")
            parser.add_argument("X-UID-Authorization", type=str, location="headers")
            parser.add_argument("X-Forwarded-Tls-Client-Cert", type=str, location="headers")
            parser.add_argument("X-Forwarded-Tls-Client-Cert-Info", type=str, location="headers")
            token = parser.parse_args().get("Authorization", "")
            uid = parser.parse_args().get("X-UID-Authorization", "")

            if uid and get_user(uid):
                return func(*f_args, **f_kwargs, session_token=None, uid=uid)
            try:
                cert = parser.parse_args().get("X-Forwarded-Tls-Client-Cert", "")
                if not cert:
                    raise Exception("not received a cert")
                pemCertRaw = unquote(cert)

                pemCert = "-----BEGIN CERTIFICATE-----"

                for i in range(0, len(pemCertRaw), 64):
                    pemCert += "\n" + pemCertRaw[i : i + 64]

                pemCert += "\n-----END CERTIFICATE-----"

                api.logger.error("%s", parser.parse_args())
                api.logger.warn("%s", pemCert.encode())

                if pemCert:
                    try:
                        uid = verifyClientCert(pemCert.encode())
                    except Exception as e:
                        api.logger.error(e)
                        raise Unauthorized("Provided client certificate is not valid.")
                    if not admin_only or is_admin(uid):
                        return func(*f_args, **f_kwargs, session_token=None, uid=uid)
            except Exception as e:
                pass
            finally:
                if token and not cert_only:
                    uid = check_session_token(token)
                    if uid and (not admin_only or is_admin(uid)):
                        return func(*f_args, **f_kwargs, session_token=token, uid=uid)

            raise Unauthorized("Session Token missing, invalid login method, or not authorized")

        return wrapped

    return decorator
