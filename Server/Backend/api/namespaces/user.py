from flask_restx import Namespace, Resource, fields
from werkzeug.exceptions import BadRequest

from ..authentication import login_required
from ..utils import get_user, get_user_id, update_user

api = Namespace("user", description="User specific operations")


@api.route("/uses_cert")
class Admin(Resource):
    @api.doc(security="session_token")
    @login_required(api, cert_only=True)
    def get(self, session_token: str, uid: str) -> dict:
        """Retrieve all information about a user,

        Returns:
            dict: saved user info
        """
        return get_user(uid).as_dict()


@api.route("/")
class User(Resource):
    @api.doc(security="session_token")
    @login_required(api)
    def get(self, session_token: str, uid: str) -> dict:
        """Get saved user information.

        Returns:
            dict: saved user info
        """
        return get_user(uid).as_dict()

    user_model = api.model(
        "User",
        {
            "lastname": fields.String,
            "firstname": fields.String,
            "email": fields.String,
            "pwd": fields.String,
            "etag": fields.String,
        },
    )

    @api.expect(user_model)
    @api.doc(security="session_token")
    @login_required(api)
    def patch(self, session_token: str, uid: str) -> dict:
        """Perform changes to own User resource.

        Returns:
            dict: adjusted user
        """
        # uid = get_user_id(session_token)
        success, result = update_user(uid, api.payload)
        # do the hash thing
        if success:
            raise BadRequest(result)
        return result
