from flask_restx import Namespace, Resource

from cryptoModule.certs import ExistingCertificate, generateCertificateForUser, getPemRootCert, getPemCrl

from ..authentication import login_required
from ..utils import get_user, get_user_id
from cryptoModule.certs import CannotFindCertificate, revokeCertificate

from base64 import b64encode

api = Namespace("certificate", description="Certificates from the CA")


@api.route("/")
class Certificate(Resource):
    @api.doc(security="session_token")
    @login_required(api)
    def post(self, session_token: str, uid: str) -> dict:
        """Get a new Certificate associated with the autheticated user.

        Returns:
            dict: raw_key, raw_cert
        """
        # uid = get_user_id(session_token)
        user = get_user(uid)
        try:
            pkcs12 = generateCertificateForUser(uid, user.email, user.firstname, user.lastname)
        except ExistingCertificate:
            return {"error": "user already has a valid certificate or key"}, 403

        api.logger.error("%s %s", str(pkcs12), b64encode(pkcs12).decode())

        return b64encode(pkcs12).decode()


@api.route("/revoke")
class Revokal(Resource):
    @api.doc(security="session_token")
    @login_required(api)
    def post(self, session_token: str, uid: str) -> dict:
        """Revoke the session associated with the current user.

        Returns:
            dict: TODO
        """
        # uid = get_user_id(session_token)
        try:
            revokeCertificate(uid)
        except CannotFindCertificate:
            return {"error": f"cannot find certificate of user {uid}"}, 404
        return {}


@api.route("/root_cert")
class RootCertificate(Resource):
    def get(self) -> dict:
        """Get the Root Certificate of the CA.

        Returns:
            dict: root CA
        """

        return {"cert": getPemRootCert().decode()}


@api.route("/crl")
class CRL(Resource):
    def get(self) -> dict:
        """Get the latest CRL of the CA.

        Returns:
            dict: root CA
        """

        return {"crl": getPemCrl().decode()}
