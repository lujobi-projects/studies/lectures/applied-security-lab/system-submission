from flask_restx import Namespace, Resource, fields
from werkzeug.exceptions import BadRequest

from ..authentication import login_required
from ..utils import check_credentials, generate_session_token

api = Namespace("login", description="Login and session handling")


@api.route("/<uid>")
class Login(Resource):
    login_model = api.model("Login", {"password": fields.String})

    @api.expect(login_model)
    def post(self, uid: str) -> dict:
        """Retrieve a new session token using either the signed challenge or the password as variable `password`.

        Args:
            uid (str): user-specific user-id

        Raises:
            BadRequest: If credentials invalid

        Returns:
            dict: new session token
        """
        if check_credentials(uid, api.payload.get("password")):
            return {"session_token": generate_session_token(uid)}
        raise BadRequest("Invalid Credentials")


@api.route("/")
class Loout(Resource):
    @api.doc(security="session_token")
    @login_required(api)
    def delete(self, session_token: str, uid: str) -> dict:
        """Delete the current session logging out the user.

        Returns:
            dict: success
        """
        return {"success": True}
