from flask_restx import Namespace, Resource

from ..authentication import login_required

api = Namespace("admin", description="Admin interface")

from api.utils import get_admin_info


@api.route("/")
class Admin(Resource):
    @api.doc(security="session_token")
    @login_required(api, cert_only=True, admin_only=True)
    def get(self, session_token: str, uid: str) -> dict:
        """Retrieve all information for the administrator.

        Returns:
            dict: admin-infos
        """
        return get_admin_info().as_dict()
