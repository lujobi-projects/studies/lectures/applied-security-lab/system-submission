from datetime import datetime
from hashlib import sha1
from secrets import token_hex
from flask_restx.fields import Boolean, Integer
from typing import Tuple

from models import db
from models.models import Session, User, AdminInfo, Admin


def add_commit(transaction: db.Model) -> None:
    db.session.add(transaction)
    db.session.commit()


def delete_commit(transaction: db.Model) -> None:
    db.session.delete(transaction)
    db.session.commit()


def update_get_serial_number() -> Integer:
    row = AdminInfo.query.order_by(AdminInfo.dateUpdated.desc()).first()
    if not row:
        return None

    AdminInfo.query.update({"serialNumber": row.serialNumber + 1, "dateUpdated": datetime.now()})
    serialNumber = row.serialNumber + 1
    db.session.commit()
    # row.serialNumber += 1
    # row.save()
    return serialNumber


def update_issued_new_certificate() -> None:
    row = AdminInfo.query.order_by(AdminInfo.dateUpdated.desc()).first()
    if not row:
        raise Exception("no admininfo found")

    # row.issuedCertificates += 1
    # row.save()
    AdminInfo.query.update({"issuedCertificates": row.issuedCertificates + 1, "dateUpdated": datetime.now()})
    db.session.commit()


def update_revoked_certificate() -> None:
    row = AdminInfo.query.order_by(AdminInfo.dateUpdated.desc()).first()
    if not row:
        raise Exception("no admininfo found")

    # row.revokedCertificates += 1
    # row.save()
    AdminInfo.query.update({"revokedCertificates": row.revokedCertificates + 1, "dateUpdated": datetime.now()})
    db.session.commit()


def get_admin_info() -> AdminInfo:
    row = AdminInfo.query.order_by(AdminInfo.dateUpdated.desc()).first()
    if not row:
        raise Exception("no admininfo found")

    return row


def is_admin(uid: str) -> Boolean:
    ad = Admin.query.filter_by(uid=uid)
    return len(ad.all()) > 0


def hash_passwd(passwd: str) -> str:
    return str(sha1(passwd.encode("utf-8")).hexdigest())


def check_credentials(uid: str, passwd: str) -> bool:
    user = User.query.filter_by(uid=uid).first()
    if not user:
        return False
    return user.pwd == hash_passwd(passwd)


def generate_session_token(uid: str) -> str:
    token = str(token_hex(64))
    while Session.query.filter_by(token=token).all():
        token = str(token_hex(64))
    add_commit(Session(user_id=uid, token=token))
    return token


def check_session_token(token: str) -> str:
    session = Session.query.filter_by(token=token).first()
    if not session:
        return None
    if datetime.now() > session.expiry_date:
        delete_commit(session)
        return None
    return session.user_id


def get_user_id(session_token: str) -> str:
    query = Session.query.filter_by(token=session_token).first()
    if query:
        return query.user_id
    return None


def get_user(uid: str) -> User:
    return User.query.filter_by(uid=uid).first()


def update_user(uid: str, to_update: dict) -> Tuple[Boolean, User]:
    user = get_user(uid)
    if not to_update:
        return False, "Missing payload"
    if not to_update.get("etag") or to_update.get("etag") != user.get_etag():
        return False, "Bad or missing Etag"

    if to_update.get("pwd"):
        to_update["pwd"] = hash_passwd(to_update.get("pwd"))

    del to_update["etag"]

    if not User.query.filter_by(uid=uid).update(to_update):
        return False, "Invalid key values"
    db.session.commit()
    return True, get_user(uid).as_dict()
