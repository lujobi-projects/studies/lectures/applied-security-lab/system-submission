from flask_restx import Api

from .namespaces.admin import api as admin
from .namespaces.certificate import api as certificate
from .namespaces.login import api as login
from .namespaces.user import api as user

authorizations = {
    "sessiontoken": {"type": "apiKey", "in": "header", "name": "Authorization"},
    "uid": {"type": "apiKey", "in": "header", "name": "X-UID-Authorization"},
}

api = Api(
    title="CA Api",
    description="CA for the iMovie coorperation",
    authorizations=authorizations,
)


api.add_namespace(admin)
api.add_namespace(certificate)
api.add_namespace(login)
api.add_namespace(user)
