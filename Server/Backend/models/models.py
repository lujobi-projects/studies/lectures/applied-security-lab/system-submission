from datetime import datetime, timedelta
from hashlib import sha1
from json import dumps

from models import db

SESSION_VALIDITY = timedelta(hours=1)
CHALLENGE_VALIDITY = timedelta(minutes=1)


class User(db.Model):
    uid = db.Column(db.String(3), nullable=False, primary_key=True)
    lastname = db.Column(db.String(256), nullable=False)
    firstname = db.Column(db.String(256), nullable=False)
    email = db.Column(db.String(256), nullable=False)
    pwd = db.Column(db.String(40), nullable=False)

    def get_etag(self) -> str:
        res_dict = {c.name: getattr(self, c.name) for c in list(self.__table__.columns)}
        return str(sha1(dumps(res_dict).encode("utf-8")).hexdigest())

    def as_dict(self) -> dict:
        res_dict = {c.name: getattr(self, c.name) for c in list(self.__table__.columns)}
        del res_dict["pwd"]
        return {**res_dict, "etag": self.get_etag()}


class Session(db.Model):
    token = db.Column(db.String(256), nullable=False, primary_key=True)
    user_id = db.Column(db.String(3), db.ForeignKey("user.uid"), nullable=False)
    expiry_date = db.Column(db.DateTime, nullable=False, default=lambda: datetime.now() + SESSION_VALIDITY)

class Admin(db.Model):
    user_id = db.Column(db.String(3), db.ForeignKey("user.uid"), nullable=False, primary_key=True)

class AdminInfo(db.Model):
    dateUpdated = db.Column(db.DateTime, nullable=False, default=lambda: datetime.now(), primary_key = True)
    serialNumber = db.Column(db.Integer, nullable=False)
    issuedCertificates = db.Column(db.Integer, nullable = False)
    revokedCertificates = db.Column(db.Integer, nullable = False)
    def as_dict(self) -> dict:
        res_dict = {c.name: getattr(self, c.name) for c in list(self.__table__.columns)}
        del res_dict['dateUpdated']
        return res_dict