# wsgi server (used in docker container)
# [bjoern](https://github.com/jonashaag/bjoern) required.

from app import app
import bjoern
from requestlogger import WSGILogger, ApacheFormatter
import logging

if __name__ == "__main__":

    logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)

    loggingapp = WSGILogger(app, [consoleHandler], ApacheFormatter())

    print("Starting bjoern on port 5000...", flush=True)
    bjoern.run(loggingapp, "0.0.0.0", 5000)
