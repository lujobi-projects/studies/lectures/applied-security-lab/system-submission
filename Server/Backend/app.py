from flask import Flask
from flask_cors import CORS
from api import api
from time import sleep

from models import db
from api.utils import hash_passwd
import argparse

import os

parser = argparse.ArgumentParser()
parser.add_argument("--genTable", action="store_true")
all_flag_values = parser.parse_args()

app = Flask(__name__)
CORS(app)

app.config.update(SQLALCHEMY_TRACK_MODIFICATIONS=False, API_KEYS=["ueli"])

user = "ca_user"
password = "Ru7xtcQspvxMZHgffBdJ"
hostname = "ca_database"
database = "ca_database"

SQLALCHEMY_DATABASE_URI = f"mysql+pymysql://{user}:{password}@{hostname}/{database}"
# SQLALCHEMY_DATABASE_URI = "sqlite:///./test.db"
SQLALCHEMY_TRACK_MODIFICATIONS = False

# print(SQLALCHEMY_DATABASE_URI)

app.config["SQLALCHEMY_DATABASE_URI"] = SQLALCHEMY_DATABASE_URI
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = SQLALCHEMY_TRACK_MODIFICATIONS

db.init_app(app)

print(all_flag_values)

try:
    from models.models import User, AdminInfo, Admin

    print("creating tables")
    with app.app_context():
        sleep(10)
        db.create_all()
        user1 = User(uid="db", lastname="Basin", firstname="David", email="db@iMovies", pwd=hash_passwd("D15Licz6"))
        user2 = User(
            uid="ps", lastname="Schaller", firstname="Patrick", email="ps@iMovies", pwd=hash_passwd("KramBamBuli")
        )
        user3 = User(
            uid="ms", lastname="Schlaepfer", firstname="Michael", email="ms@iMovies", pwd=hash_passwd("MidbSvlJ")
        )
        user4 = User(
            uid="a3", lastname="Anderson", firstname="Andres Alan", email="and@iMovies", pwd=hash_passwd("Astrid")
        )
        db.session.add_all((user1, user2, user3, user4))

        db.session.commit()

        db.session.add(AdminInfo(serialNumber=0x100010, issuedCertificates=0, revokedCertificates=0))

        db.session.commit()
        db.session.add(Admin(user_id="ps"))

        db.session.commit()
except Exception as e:
    print(e)


api.init_app(app)
