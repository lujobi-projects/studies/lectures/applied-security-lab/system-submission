# Applied Security Lab

To install:

* `sudo apt install libnss3-tools`

* `./mkcert -cert-file ../applied-security-lab/certs/local-cert.pem -key-file ../applied-security-lab/certs/local-key.pem "docker.localhost" "*.docker.localhost" "domain.local" "*.domain.local"`

* `python3 -m venv venv`
* `source venv/bin/activate`