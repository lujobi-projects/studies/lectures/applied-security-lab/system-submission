#!/bin/bash
# directories to backup: 
# - /var/log
# - database folder
# - /home/vagrant/code/Backend/certs
# - 
# 
# 
#

sudo rsync -av /var/log/ vagrant@backup.appsec:backup/log -e "ssh -i $HOME/.ssh/server_id"
sudo rsync -av /home/vagrant/.certs/user_certs vagrant@backup.appsec:backup/certs -e "ssh -i $HOME/.ssh/server_id"
sudo rsync -av /home/vagrant/.certs/user_keys vagrant@backup.appsec:backup/certs -e "ssh -i $HOME/.ssh/server_id"
sudo rsync -av /home/vagrant/applied-security-lab/Server/Database vagrant@backup.appsec:backup/Database -e "ssh -i $HOME/.ssh/server_id"


