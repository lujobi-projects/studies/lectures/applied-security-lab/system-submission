import * as React from "react";
import Typography from "@mui/material/Typography";
import {
  Alert,
  Button,
  Divider,
  Snackbar,
  TextField,
} from "@mui/material";
import { useState } from "react";
import axios from "axios";
import { get_authentication } from "../utils";
import { API_ADDRESS } from "../config";

import CertFunctions from "../components/CertFunctions";

export default function App() {
  const [user, set_user] = useState({
    uid: "tmp",
    lastname: "tmp",
    firstname: "tmp",
    email: "tmp",
    etag: "tmp",
  });

  const [uid, set_uid] = useState("");
  const [first_name, set_first_name] = useState("");
  const [last_name, set_last_name] = useState("");
  const [email, set_email] = useState("");
  const [password, set_password] = useState("");
  const [password2, set_password2] = useState("");

  const [password_err, set_password_err] = useState(true);
  const [password2_err, set_password2_err] = useState(true);
  const [error, set_error] = useState(false);

  React.useEffect(() => {
    set_password2_err(password != password2);
  }, [password, password2]);
  React.useEffect(() => {
    set_password_err(password.length <= 3 && password.length != 0);
  }, [password, password2]);
  React.useEffect(() => {
    set_error(password_err && password2_err);
  }, [password_err, password2_err]);

  React.useEffect(() => {
    axios
      .get(`${API_ADDRESS}/user/`, get_authentication())
      .then((e) => {
        set_user(e.data);
      })
      .catch((_) => window.location.replace(window.location.origin));
  }, []);

  React.useEffect(() => {
    set_uid(user.uid);
    set_first_name(user.firstname);
    set_last_name(user.lastname);
    set_email(user.email);
  }, [user]);

  const [req_error, set_req_error] = useState(false);
  const [req_succ, set_req_succ] = useState(false);

  const handleClose_err = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    set_req_error(false);
  };
  const handleClose_succ = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    set_req_succ(false);
  };

  const update_user = () => {
    const to_update = {};
    first_name && (to_update["firstname"] = first_name);
    last_name && (to_update["lastname"] = last_name);
    email && (to_update["email"] = email);
    password && (to_update["pwd"] = password);
    to_update["etag"] = user.etag;
    axios
      .patch(`${API_ADDRESS}/user/`, to_update, get_authentication())
      .then((_) => {
        set_req_succ(true);
      })
      .catch((e) => {
        set_req_error(true);
        console.log(e);
      });
  };

  return (
    <>
      <Typography variant="h4" component="h1" gutterBottom>
        User - {user.uid}
      </Typography>
      <TextField
        id="standard-basic"
        label="UID"
        variant="standard"
        fullWidth
        value={uid}
        disabled
      />
      <TextField
        id="standard-basic"
        label="First Name"
        variant="standard"
        fullWidth
        value={first_name}
        onChange={(e) => set_first_name(e.target.value)}
      />
      <TextField
        id="standard-basic"
        label="Last Name"
        variant="standard"
        fullWidth
        value={last_name}
        onChange={(e) => set_last_name(e.target.value)}
      />
      <TextField
        id="standard-basic"
        label="Email"
        variant="standard"
        fullWidth
        value={email}
        onChange={(e) => set_email(e.target.value)}
      />
      <TextField
        id="standard-basic"
        label="Password"
        variant="standard"
        type="password"
        fullWidth
        onChange={(e) => set_password(e.target.value)}
        error={password_err}
        helperText="Password length needs to be larger than 3"
      />
      <TextField
        id="standard-basic"
        label="Password (second time)"
        variant="standard"
        type="password"
        fullWidth
        onChange={(e) => set_password2(e.target.value)}
        error={password2_err}
      />

      <Button
        fullWidth
        variant="outlined"
        style={{ marginTop: "1rem" }}
        disabled={error}
        onClick={update_user}
      >
        Update
      </Button>

      <Divider style={{ marginTop: "1rem", marginBottom: "1rem" }} />

      <CertFunctions />
      <Snackbar
        open={req_error}
        autoHideDuration={6000}
        onClose={handleClose_err}
      >
        <Alert
          onClose={handleClose_err}
          severity="error"
          sx={{ width: "100%" }}
        >
          ERROR: {req_error}
        </Alert>
      </Snackbar>
      <Snackbar
        open={req_succ}
        autoHideDuration={6000}
        onClose={handleClose_succ}
      >
        <Alert
          onClose={handleClose_succ}
          severity="success"
          sx={{ width: "100%" }}
        >
          Changes successful.
        </Alert>
      </Snackbar>
    </>
  );
}
