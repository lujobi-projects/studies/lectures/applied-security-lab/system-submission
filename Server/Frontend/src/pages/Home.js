import * as React from "react";
import Typography from "@mui/material/Typography";
import axios from "axios";
import { API_ADDRESS } from "../config";
import { get_authentication } from "../utils";

export default function App() {
  const [user, set_user] = React.useState({});

  React.useEffect(() => {
    axios
      .get(`${API_ADDRESS}/user/`, get_authentication())
      .then((e) => {
        set_user(e.data);
      })
      .catch((e) => {console.log(e)});
  }, []);

  return (
    <>
      {user.firstname ? (
        <>
          <Typography variant="h4" component="h1" gutterBottom>
            Home
          </Typography>
          <Typography variant="h6" component="h1" gutterBottom>
            Hello {user.firstname}
          </Typography>
        </>
      ) : (
        <>
          <Typography variant="h4" component="h1" gutterBottom>
            Home - not logged in
          </Typography>
          <Typography variant="h6" component="h1" gutterBottom>
            Please log in either via Cert or password
          </Typography>
        </>
      )}
    </>
  );
}
