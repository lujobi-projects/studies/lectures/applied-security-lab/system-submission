import * as React from "react";
import Typography from "@mui/material/Typography";
import { Alert, Button, Snackbar, TextField } from "@mui/material";
import { useState } from "react";
import { API_ADDRESS } from "../config";

import axios from "axios";

export default function App() {
  const [uid, set_uid] = useState("");
  const [password, set_password] = useState("");
  const [open, setOpen] = React.useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const login_failed = () => {
    setOpen(true);
  };

  const login_successful = (e) => {
    window.localStorage.setItem("session_token", e.data.session_token);
    console.log(window.location);
    window.location.replace(window.location.origin);
  };

  const login = () => {
    axios
      .post(`${API_ADDRESS}/login/${uid}`, { password })
      .then(login_successful)
      .catch(login_failed);
  };

  return (
    <>
      <Typography variant="h4" component="h1" gutterBottom>
        Login
      </Typography>
      <TextField
        id="standard-basic"
        label="UID"
        variant="standard"
        fullWidth
        onChange={(e) => set_uid(e.target.value)}
      />
      <TextField
        id="standard-basic"
        label="Password"
        variant="standard"
        type="password"
        fullWidth
        onChange={(e) => set_password(e.target.value)}
      />

      <Button
        fullWidth
        variant="contained"
        style={{ marginTop: "1rem" }}
        onClick={login}
      >
        Login
      </Button>
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error" sx={{ width: "100%" }}>
          Login Failed. Username or password invalid.
        </Alert>
      </Snackbar>
    </>
  );
}
