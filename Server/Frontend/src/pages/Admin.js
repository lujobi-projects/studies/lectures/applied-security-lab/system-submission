import * as React from "react";
import Typography from "@mui/material/Typography";
import { Card, CardContent, Grid } from "@mui/material";
import axios from "axios";
import { API_ADDRESS } from "../config";
import { get_authentication } from "../utils";

export default function App() {
  const [info, set_info] = React.useState({
    serialNumber: "tmp",
    issuedCertificates: "tmp",
    revokedCertificates: "tmp",
  });

  React.useEffect(() => {
    axios
      .get(`${API_ADDRESS}/admin/`, get_authentication())
      .then((e) => {
        set_info(e.data);
      })
      .catch((_) => window.location.replace(window.location.origin));
  }, []);

  return (
    <Grid sx={{ flexGrow: 1 }} container spacing={2}>
      <Grid item xs={12}>
        <Grid container justifyContent="center" spacing={2}>
          <Grid item xs={12}>
            <Card>
              <CardContent>
                <Typography variant="h5" component="div">
                  Serial Number
                </Typography>
                <Typography variant="h6" component="div">
                  {info.serialNumber}
                </Typography>
                <Typography variant="body2">Current Serial number</Typography>
              </CardContent>
            </Card>
          </Grid>
          <Grid item xs={12}>
            <Card>
              <CardContent>
                <Typography variant="h5" component="div">
                  Issued Certs
                </Typography>
                <Typography variant="h6" component="div">
                  {info.issuedCertificates}
                </Typography>
                <Typography variant="body2">
                  Number of issued certificates
                </Typography>
              </CardContent>
            </Card>
          </Grid>
          <Grid item xs={12}>
            <Card>
              <CardContent>
                <Typography variant="h5" component="div">
                  Revoked Certs
                </Typography>
                <Typography variant="h6" component="div">
                  {info.revokedCertificates}
                </Typography>
                <Typography variant="body2">
                  Number of certifcates on the revoke list
                </Typography>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </Grid>
    </Grid>

    // <>
    // <Card >
    //   <CardContent>
    //     <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
    //       Word of the Day
    //     </Typography>
    //     <Typography sx={{ mb: 1.5 }} color="text.secondary">
    //       adjective
    //     </Typography>
    //     <Typography variant="body2">
    //       well meaning and kindly.
    //       <br />
    //       {'"a benevolent smile"'}
    //     </Typography>
    //   </CardContent>
    // </Card>
    //   <Card >
    //     <CardContent>
    //       <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
    //         Word of the Day
    //       </Typography>
    //       <Typography sx={{ mb: 1.5 }} color="text.secondary">
    //         adjective
    //       </Typography>
    //       <Typography variant="body2">
    //         well meaning and kindly.
    //         <br />
    //         {'"a benevolent smile"'}
    //       </Typography>
    //     </CardContent>
    //   </Card>
    //   <Card >
    //     <CardContent>
    //       <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
    //         Word of the Day
    //       </Typography>
    //       <Typography sx={{ mb: 1.5 }} color="text.secondary">
    //         adjective
    //       </Typography>
    //       <Typography variant="body2">
    //         well meaning and kindly.
    //         <br />
    //         {'"a benevolent smile"'}
    //       </Typography>
    //     </CardContent>
    //   </Card>

    // </>
  );
}
