import * as React from "react";
import { useState, useEffect } from "react";
import { API_ADDRESS } from "./config";

import axios from "axios";

import { Routes, Route } from "react-router-dom";

import Layout from "./components/Layout";
import Login from "./pages/Login";
import Admin from "./pages/Admin";
import Home from "./pages/Home";
import User from "./pages/User";
import { get_authentication } from "./utils";

const App = () => {
  const [is_logged_in, set_is_logged_in] = useState(true);
  const [logged_in_via_cert, set_logged_in_via_cert] = useState(false);
  const [is_admin, set_is_admin] = useState(true);

  useEffect(() => {
    axios.get(`${API_ADDRESS}/user/`, get_authentication()).catch((_) => {
      set_is_logged_in(false);
    })
  }, []);

  useEffect(() => {
    axios.get(`${API_ADDRESS}/admin/`, get_authentication()).then((_) => {
      set_is_admin(false);
    })
  }, []);

  useEffect(() => {
    axios.get(`${API_ADDRESS}/user/uses_cert`, get_authentication()).then((_) => {
      set_logged_in_via_cert(false);
    })
  }, []);

  return (
    <Routes>
      <Route
        path="/"
        element={
          <Layout
            name="Home"
            menu={is_logged_in}
            is_admin={is_admin}
            login_button={!is_logged_in}
            logout_button={is_logged_in && !logged_in_via_cert}
          >
            <Home />
          </Layout>
        }
      />
      {!is_logged_in && (
        <Route
          path="/login"
          element={
            <Layout
              name="Login"
              menu={is_logged_in}
              is_admin={is_admin}
              logout_button={is_logged_in && !logged_in_via_cert}
            >
              <Login />
            </Layout>
          }
        />
      )}
      {is_admin && is_logged_in && (
        <Route
          path="/admin"
          element={
            <Layout
              name="Admin"
              menu={is_logged_in}
              is_admin={is_admin}
              logout_button={is_logged_in && !logged_in_via_cert}
            >
              <Admin />
            </Layout>
          }
        />
      )}
      {is_logged_in && (
        <Route
          path="/user"
          element={
            <Layout
              name="User"
              menu={is_logged_in}
              is_admin={is_admin}
              logout_button={is_logged_in && !logged_in_via_cert}
            >
              <User />
            </Layout>
          }
        />
      )}
    </Routes>
  );
};
export default App;
