import * as React from "react";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import { useState } from "react";
import Box from "@mui/material/Box";
import AppBar from "@mui/material/AppBar";
import BottomNavigation from '@mui/material/BottomNavigation';

import Toolbar from "@mui/material/Toolbar";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";

import { Link } from "react-router-dom";
import { Menu, MenuItem } from "@mui/material";
import axios from "axios";
import { API_ADDRESS } from "../config";
import { get_authentication } from "../utils";

export default function Layout({
  name,
  children,
  is_admin,
  login_button,
  logout_button,
  menu,
}) {
  const [menuOpen, setMenuOpen] = useState(false);
  const [anchorEl, setAnchorEl] = useState();

  const recordButtonPosition = (event) => {
    setAnchorEl(event.currentTarget);
    setMenuOpen(true);
  };

  let closeMenu = () => {
    setMenuOpen(false);
  };

  const logout = async () => {
    window.localStorage.removeItem("session_token");
    await axios
      .delete(`${API_ADDRESS}/login/`, get_authentication())
      .catch((e) => { console.log(e) });
    window.location.replace(window.location.origin);
  };

  return (
    <>
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static">
          <Toolbar>
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
              onClick={recordButtonPosition}
              sx={{ mr: 2 }}
              disabled={!menu}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              {name}
            </Typography>
            {login_button && (
              <Link to="/login">
                <Button variant="contained" color="error">
                  Login
                </Button>
              </Link>
            )}
            {logout_button && (
              <Button variant="contained" color="error" onClick={logout}>
                Logout
              </Button>
            )}
          </Toolbar>
        </AppBar>
        <Menu anchorEl={anchorEl} open={menuOpen} onClose={closeMenu}>
          <Link to="/">
            <MenuItem>
              <Button onClick={closeMenu}>Home </Button>
            </MenuItem>
          </Link>
          <Link to="/user">
            <MenuItem>
              <Button onClick={closeMenu}>User </Button>
            </MenuItem>
          </Link>
          {is_admin && <Link to={is_admin ? "/admin" : "/"}>
            <MenuItem>
              <Button onClick={closeMenu} disabled={!is_admin}>
                Admin
              </Button>
            </MenuItem>
          </Link>}
        </Menu>
      </Box>
      <Container maxWidth="xs">
        <Box sx={{ my: 6 }}>{children}</Box>
      </Container>
      <BottomNavigation>
        <Typography variant="h5" component="h1" gutterBottom>
          Find the API docs <a href="http://api.server.appsec">here</a>
        </Typography>
      </BottomNavigation>
    </>
  );
}
