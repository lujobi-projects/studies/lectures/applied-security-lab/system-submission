import * as React from "react";
import Typography from "@mui/material/Typography";
import {
  Alert,
  Button,
  Grid,
  Snackbar,
} from "@mui/material";
import { useState } from "react";
import axios from "axios";
import { get_authentication } from "../utils";
import { API_ADDRESS } from "../config";

import { saveAs } from "file-saver";

const App = () => {
  const [req_error, set_req_error] = useState(false);
  const [req_issue, set_req_issue] = useState("");
  const [req_succ, set_req_succ] = useState(false);

  const base64toByteArray = base64String => {
    const byteArray = Uint8Array.from(
      atob(base64String)
        .split('')
        .map(char => char.charCodeAt(0))
    );
    return byteArray
  };

  const save_cert = (content) => {
    console.log(content)
    var blob = new Blob([base64toByteArray(content)], {
      type: "application/x-pkcs12",
    });
    console.log(blob)
    saveAs(blob, "cert.p12");
  };

  const get_cert = () => {
    axios
      .post(`${API_ADDRESS}/certificate/`, {}, get_authentication())
      .then((e) => {
        save_cert(e.data);
      })
      .catch((e) => {
        console.log(e);
        set_req_succ(false);
        set_req_error(true);
        set_req_issue(e.message);
      });
  };
  const revoke_cert = () => {
    axios
      .post(`${API_ADDRESS}/certificate/revoke`, {}, get_authentication())
      .then(() => {
        set_req_succ(true);
        set_req_error(false);
      })
      .catch((e) => {
        console.log(e);
        set_req_succ(false);
        set_req_error(true);
        set_req_issue(e.message);
      });
  };

  const handleClose_err = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    set_req_error(false);
  };

  const handleClose_succ = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    set_req_succ(false);
  };

  return (
    <>
      <Typography variant="h5" component="h1" gutterBottom>
        Certificate Functions
      </Typography>
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <Button variant="contained" onClick={get_cert}>
            New
          </Button>
        </Grid>
        <Grid item xs={6}>
          <Button variant="contained" onClick={revoke_cert}>
            Revoke
          </Button>
        </Grid>
      </Grid>
      <Snackbar
        open={req_error}
        autoHideDuration={6000}
        onClose={handleClose_err}
      >
        <Alert
          onClose={handleClose_err}
          severity="error"
          sx={{ width: "100%" }}
        >
          ERROR: {req_issue}
        </Alert>
      </Snackbar>
      <Snackbar
        open={req_succ}
        autoHideDuration={6000}
        onClose={handleClose_succ}
      >
        <Alert
          onClose={handleClose_succ}
          severity="success"
          sx={{ width: "100%" }}
        >
          Changes successful.
        </Alert>
      </Snackbar>
    </>
  );
};

export default App;
