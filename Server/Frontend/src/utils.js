export const get_authentication = () => {
  return {
    headers: { Authorization: window.localStorage.getItem("session_token") },
  };
};
