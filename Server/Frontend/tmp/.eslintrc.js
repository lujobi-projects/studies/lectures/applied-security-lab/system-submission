module.exports = {
  extends: [
    'airbnb-base',
    'plugin:react/recommended',
    'plugin:prettier/recommended',
  ],
  env: {
    browser: true,
    node: true,
  },
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  rules: {
    'no-multi-str': 0,
    'no-underscore-dangle': 0,
    'no-console': 0,
    'import/prefer-default-export': 0,
    'import/no-extraneous-dependencies': [1, { peerDependencies: true }],
    'max-classes-per-file': [1, 2],
    camelcase: 0,
    'react/jsx-uses-react': 'error',
    'react/jsx-uses-vars': 'error',
    'prettier/prettier': 'error',
    'react-hooks/rules-of-hooks': 'error', // Checks rules of Hooks
    'react-hooks/exhaustive-deps': 'warn', // Checks effect dependencies
  },
  plugins: ['react', 'prettier', 'react-hooks', 'html'],
  // Activate the resolver plugin, required to recognize the 'config' resolver
  settings: {
    react: {
      version: 'detect',
    },
    linkComponents: [
      // Components used as alternatives to <a> for linking, eg. <Link to={ url } />
      'Hyperlink',
      { name: 'Link', linkAttribute: 'to' },
    ],
    'html/html-extensions': ['.html', '.mustache'],
    'import/resolver': {
      webpack: {},
      alias: [
        ['config', './config.development.js'],
        ['~store', './src/store'],
        ['~utils', './src/utils'],
        ['~images', './src/images'],
        ['~context', './src/context'][('~components', './src/components')],
      ],
    },
  },
}
