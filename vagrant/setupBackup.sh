cd

mkdir -p backup
mkdir -p backup/log
mkdir -p backup/certs

cat ~/.ssh/server_id.pub >> ~/.ssh/authorized_keys
cat ~/.ssh/admin_id.pub >> ~/.ssh/authorized_keys

rm ~/.ssh/server_id.pub
rm ~/.ssh/admin_id.pub