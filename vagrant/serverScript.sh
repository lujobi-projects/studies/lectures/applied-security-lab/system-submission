# hardening
# firewall
# any process that receives input from the outside poses a potential security risk

# so it doesn't expect us to interact with it
export DEBIAN_FRONTEND=noninteractive

apt-get install -y rsync

# make sure everything has the latest (security) updates
apt-get -y upgrade

# fail2ban -> block ip addresses after too many login attempts
apt-get install -y fail2ban

# auditd to collect logs
apt-get -y install auditd
systemctl restart auditd.service


# to make iptables persistent after reboot
echo iptables-persistent iptables-persistent/autosave_v4 boolean true | debconf-set-selections
echo iptables-persistent iptables-persistent/autosave_v6 boolean true | debconf-set-selections
apt-get -y install iptables-persistent

# remove unneccessary packages
apt-get purge --auto-remove --yes telnet
apt-get purge --auto-remove --yes netcat-traditional
apt-get purge --auto-remove --yes nano
# apt-get purge --auto-remove --yes gzip system motzt
# apt-get purge --auto-remove --yes bzip2 fail2ban
apt-get purge --auto-remove --yes memcached
# apt-get purge --auto-remove --yes mime-support fail2ban
apt-get purge --auto-remove --yes postfix
apt-get purge --auto-remove --yes postfix-cdb
apt-get purge --auto-remove --yes whiptail


# configure fail2ban to ban ips after 5 retrys
cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local

echo "[sshd]
enabled = true
port = ssh
filter = sshd
logpath = /var/log/auth.log
maxretry = 5
findtime = 300
bantime = 600" > /etc/fail2ban/jail.local

sudo systemctl restart fail2ban.service


# harden ssh
echo "UsePrivilegeSeparation sandbox
StrictModes yes
GatewayPorts no
PermitUserRC no
PermitTunnel no
AllowStreamLocalForwarding no
AllowTcpForwarding no
AllowAgentForwarding no
GSSAPIKeyExchange no
GSSAPIAuthentication no
HostbasedAuthentication no
KerberosAuthentication no
KbdInteractiveAuthentication no
PasswordAuthentication no
PubkeyAuthentication yes
AllowUsers **
AllowGroups **
DenyUsers nobody
DenyGroups nobody
X11Forwarding no
PermitRootLogin no
PermitUserEnvironment no
MaxSessions 10
LoginGraceTime 60
ClientAliveInterval 300
ClientAliveCountMax 0
MaxAuthTries 4
" >> /etc/ssh/sshd_config
systemctl restart ssh.service


# disable usb & firewire stuff
echo "install usb-storage /bin/true" >> /etc/modprobe.d/disable-usb-storage.conf
echo "blacklist firewire-core" >> /etc/modprobe.d/firewire.conf
echo "blacklist thunderbolt" >> /etc/modprobe.d/thunderbolt.conf


# disable services
echo "install tipc /bin/true" > /etc/modprobe.d/tipc.conf
echo "install dccp /bin/true" > /etc/modprobe.d/dccp.conf
echo "install rds /bin/true" > /etc/modprobe.d/rds.conf
echo "install sctp /bin/true" > /etc/modprobe.d/sctp.conf



# disable source routing stuff
/sbin/sysctl -w net.ipv4.conf.all.send_redirects=0
/sbin/sysctl -w net.ipv4.conf.default.send_redirects=0
/sbin/sysctl -w net.ipv4.conf.default.accept_redirects=0
/sbin/sysctl -w net.ipv6.conf.all.accept_redirects=0
/sbin/sysctl -w net.ipv6.conf.default.accept_redirects=0
/sbin/sysctl -w net.ipv4.conf.all.secure_redirects=0
/sbin/sysctl -w net.ipv4.conf.default.secure_redirects=0
/sbin/sysctl -w net.ipv4.conf.all.log_martians=1
/sbin/sysctl -w net.ipv4.conf.default.log_martians=1
/sbin/sysctl -w net.ipv4.conf.all.rp_filter=1
/sbin/sysctl -w net.ipv4.conf.default.rp_filter=1
/sbin/sysctl -w net.ipv4.ip_forward=0
/sbin/sysctl -w net.ipv4.conf.all.rp_filter=1
/sbin/sysctl -w net.ipv4.conf.default.rp_filter=1
/sbin/sysctl -w net.ipv4.conf.default.log_martians=1
/sbin/sysctl -w net.ipv4.conf.all.log_martians=1
/sbin/sysctl -w net.ipv4.conf.default.secure_redirects=0
/sbin/sysctl -w net.ipv4.conf.all.secure_redirects=0
/sbin/sysctl -w net.ipv6.conf.default.accept_redirects=0
/sbin/sysctl -w net.ipv6.conf.all.accept_redirects=0
/sbin/sysctl -w net.ipv4.conf.default.accept_redirects=0
/sbin/sysctl -w net.ipv4.ip_forward=0
/sbin/sysctl -w net.ipv4.conf.default.send_redirects=0
/sbin/sysctl -w net.ipv4.conf.all.send_redirects=0


# fix permissions
chmod 400 /boot/grub/grub.cfg
chmod 640 /var/log/wtmp /var/log/vboxadd-install.log /var/log/dpkg.log /var/log/btmp /var/log/vboxadd-setup.log /var/log/lastlog /var/log/vboxadd-setup.log.2 /var/log/alternatives.log /var/log/faillog
chmod 640 /var/log/installer/hardware-summary /var/log/installer/syslog /var/log/installer/status /var/log/installer/cdebconf/questions.dat /var/log/installer/cdebconf/templates.dat
chmod 640 /var/log/installer/partman /var/log/installer/Xorg.0.log /var/log/installer/lsb-release /var/log/apt/history.log /var/log/apt/eipp.log.xz /var/log/vboxadd-setup.log.1
chmod 600 /etc/crontab /etc/ssh/sshd_config
chmod 700 /etc/cron.hourly /etc/cron.daily /etc/cron.weekly /etc/cron.monthly /etc/cron.d
chmod o-rx /home/vagrant
chmod 640 /var/log/apt/history.log
chmod 640 /var/log/apt/eipp.log.xz
chmod 640 /var/log/lastlog
chmod 640 /var/log/vboxadd-setup.log
chmod 640 /var/log/btmp
chmod 640 /var/log/wtmp



# allow http and https
iptables -A INPUT -i eth0 -p tcp --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -i eth0 -p tcp --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT

# allow all ssh
iptables -A INPUT -i eth0 -p tcp --dport 22 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -o eth0 -p tcp --sport 22 -m state --state ESTABLISHED -j ACCEPT

# allow rsync --dport? (ip should be correct)
iptables -A INPUT -i eth0 -p tcp -s 192.168.56.201/32 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -o eth0 -p tcp --sport 873 -m state --state ESTABLISHED -j ACCEPT

# allow established connections (coming from inside)
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

# set DROP policy
iptables -P INPUT DROP

# save rules in case of reboot
netfilter-persistent save

# bash timeout
echo "export TMOUT=6000" >> .bashrc

# try again at the end?
chmod 600 /etc/group- /etc/shadow- /etc/passwd-


# fix the whole sudothing and userthing
###### by hand before handing in:
# (sudo su)
# adduser sysadmin
# ( pw: q@*DmTeJemYt@!LxVNna4t8. )
##################

echo "usermod -aG sudo sysadmin
cd /home/vagrant/.ssh
mkdir /home/sysadmin/.ssh
chmod o-rx /home/sysadmin
head -2 authorized_keys | tail -1 > /home/sysadmin/.ssh/authorized_keys
cp authorized_keys tmp
head -1 tmp > authorized_keys
rm tmp
rm /etc/sudoers.d/vagrant
" > /home/vagrant/after_usercreation.sh

######### execute this script as root after creating the sysadmin user



# # apply hardening scripts
# git clone https://github.com/ovh/debian-cis.git && cd debian-cis
# sudo cp debian/default /etc/default/cis-hardening
# sudo sed -i "s#CIS_ROOT_DIR=.*#CIS_ROOT_DIR='$(pwd)'#" /etc/default/cis-hardening
# sudo bin/hardening.sh --audit-all
