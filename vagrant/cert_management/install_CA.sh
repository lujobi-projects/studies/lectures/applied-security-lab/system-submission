
export CAROOT="/home/vagrant/.certs"

sudo apt-get update
sudo apt-get install -y curl libnss3-tools

if ! command -v mkcert -h &> /dev/null
then
    curl -s https://api.github.com/repos/FiloSottile/mkcert/releases/latest | grep browser_download_url  | grep linux-amd64 | cut -d '"' -f 4 | wget -qi -
    mv mkcert-v*-linux-amd64 mkcert
    chmod a+x mkcert
    sudo mv mkcert /usr/local/bin/
fi
# if ! command -v go &> /dev/null
# then
#     wget -q https://golang.org/dl/go1.17.3.linux-amd64.tar.gz
#     rm -rf /usr/local/go && tar -C /usr/local -xzf go1.17.3.linux-amd64.tar.gz
#     rm  go1.17.3.linux-amd64.tar.gz
# fi

# export PATH=$PATH:/usr/local/go/bin

# git clone https://github.com/FiloSottile/mkcert && cd mkcert
# go build -ldflags "-X main.Version=$(git describe --tags)"

mkcert -CAROOT

mkcert -install