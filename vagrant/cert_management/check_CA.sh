SCRIPT=$(greadlink -f "$0")
BASEDIR=$(dirname "$SCRIPT")

echo "$BASEDIR"

cd $BASEDIR

export CAROOT="$BASEDIR"
export TRUST_STORES=""

if [ ! -f "$BASEDIR/rootCA.pem" ]; then
    echo $(mkcert -CAROOT)
    mkcert
    mkcert -key-file "$BASEDIR/server-key.pem" -cert-file "$BASEDIR/server-cert.pem" server.appsec "*.server.appsec"
else
    echo "rootCA already exists"
fi

if [ ! -f "$BASEDIR/admin_id" ]; then
    ssh-keygen -f admin_id -N ""
else
    echo "admin key already exists"
fi

if [ ! -f "$BASEDIR/server_id" ]; then
    ssh-keygen -f server_id -N ""
else
    echo "server key already exists"
fi

if [ ! -f "$BASEDIR/encryption_key" ]; then
    ssh-keygen -t rsa -b 1024 -m pem -f encryption_key -N ""
else
    echo "encryption key already exists"
fi