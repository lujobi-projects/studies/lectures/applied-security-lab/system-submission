
cat ~/.ssh/admin_id.pub >> ~/.ssh/authorized_keys

rm ~/.ssh/admin_id.pub

cat >> ~/.ssh/config <<- EOM
Host backup.appsec
    User vagrant
    IdentityFile ~/.ssh/server_id
EOM

crontab -r

(crontab -l -u vagrant 2>/dev/null; echo "1 02 * * * sh /home/vagrant/code/backupScript.sh") | crontab -u vagrant -